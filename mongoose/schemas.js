/**
 * Created by user on 25/07/2017.
 */

const mongoose = require('mongoose');

var schemaArticle = mongoose.Schema({
    content: String,
    user_id: String,
    title: String,
    category: String,
    tags: Array
});
var Article = mongoose.model("Article", schemaArticle);

var schemas = {
    Article: Article
};

module.exports = schemas;
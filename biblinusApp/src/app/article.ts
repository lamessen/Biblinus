import {User} from './user';

interface IArticle {
  content: String;
  user_id: String;
  title: String;
  category: String;
  tags: Array<String>;
}

export {IArticle};

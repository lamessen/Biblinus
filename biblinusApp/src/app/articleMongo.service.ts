import {Injectable} from '@angular/core';
import {IArticle} from './article';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';


const url = 'http://localhost:3000/datas/Article';

@Injectable()
export class ArticleMongoService {
  constructor(private http: HttpClient) {}

  getArticles(): Observable<IArticle[]> {
    return this.http.get<IArticle[]>(url);
  }
}

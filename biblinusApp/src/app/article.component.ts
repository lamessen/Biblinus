import {Component, OnInit} from '@angular/core';
import {ArticleMongoService} from './articleMongo.service';
import {IArticle} from './article';
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
  providers: [ArticleMongoService]
})
export class ArticleComponent implements OnInit {
  articles: Array<IArticle> = [];
  service: ArticleMongoService = null;

  constructor(service: ArticleMongoService) {
    this.service = service;
  }

  ngOnInit() {
    this.service.getArticles().subscribe(resp => {
      console.log(resp);
      this.articles = resp;
    });
  }

}

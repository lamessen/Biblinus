import { TestBed, inject } from '@angular/core/testing';

import { ArticleMongoService } from './articleMongo.service';

describe('ArticleMongoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArticleMongoService]
    });
  });

  it('should be created', inject([ArticleMongoService], (service: ArticleMongoService) => {
    expect(service).toBeTruthy();
  }));
});

// Déclarations des modules
const express = require('express');
const path = require('path');
const fs = require('fs');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const schemas = require('./mongoose/schemas.js');
const request = require('request');

// Déclaration du serveur
const hostname = '127.0.0.1';
const port = 3000;
const db_path = 'mongodb://localhost/db'
const db_port = 27017;

// Repertoire
const commons = path.join(__dirname, 'datas');

// Lancement de express
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Lancement de la connection mongo
mongoose.connect(db_path, { useMongoClient: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Connected to mongo!");
});


// Génération du HTML de sortie
function generateHTML(headers, body){
    var html = '<!DOCTYPE HTML>\n\t<head>\n';
    headers.forEach(balise=>html += '\t\t' + balise + '\n');
    html += '\t</head>\n\t<body>';
    body.forEach(balise=>html += '\t\t' + balise + '\n');
    html += '\t</body>\n</html>';
    return html;
}

// Traitement des demandes
app.route(['/commons', '/commons/*']).get((req, res, next)=>{
    if(!req.params || !req.params[0]){
        req.dest = commons;
    }else{
        req.dest = path.join(commons, req.params[0]);
    }
    fs.exists(req.dest, exists=>{
        if(exists){
            next();
        }else{
            res.status(404).end();
        }
    });
}, (req, res, next)=>{
        fs.lstat(req.dest, (err, stats)=>{
            if(stats.isDirectory()){
            next();
        }else{
            res.sendFile(path.join(commons, req.params[0]));
        }
    });
}, (req, res)=>{
        fs.readdir(req.dest, (err, files)=>{
            if(err){
                res.status(500).end();
                return;
            }
            var format = {
                text: function(){
                    res.setHeader('Content-Type', 'text/plain; charset=utf-8')
                        .send(files.toString());
                },
                json: function(){
                    res.json(files);
                },
                html: function(){
                    res.setHeader('Content-Type', 'text/html; charset=utf-8');
                    var headers = [
                        '<meta http-equiv="content-type" content="text/html; />',
                        '<title>' + req.title + '</title>'
                    ];
                    var list = files.map(file=>'<li><a href="' + req.path + '/' + file + '">' + file + '</a></li>');
                    var up = req.path.substr(0, req.path.lastIndexOf('/'));
                    if(up)
                        list.unshift('<li><a href="' + up + '">..</a></li>');
                    list.unshift('<ul>');
                    list.push('</ul>');
                    res.send(generateHTML(headers, list));
                }
            };
            if(req.query && req.query.format){
                format[req.query.format]();
            }else{
                res.format(format);
            }
        });
});

//Accès BdD
app.route('/datas/:collection').post(function (req, res) {
    var params = req.json;
    if(params && req.params.collection && schemas[req.params.collection]){
        var model = schemas[req.params.collection];
        model.find(params, function(err, results){
            res.send(results).end();
        });
    }else{
        res.end(403);
    }
}).get(function(req, res){
    if(req.params.collection && schemas[req.params.collection]) {
        var model = schemas[req.params.collection];
        model.find({}, function (err, results) {
            res.send(results).end();
        });
    }else{
        res.end(403);
    }
});

//Accés application
app.route(['/','/*']).get(function(req, res){
    var url = 'http://localhost:4200/';
    if(req.params[0]) url += req.params[0];
    request(url).pipe(res);
});

app.listen(port, function () {
    console.log(`Example app listening on port ${port}!`);
});